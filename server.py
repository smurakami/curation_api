from flask import Flask, request, jsonify
import random
import json

from scipy.spatial import KDTree
from sklearn.preprocessing import StandardScaler
import numpy as np


app = Flask(__name__)


# データのロード
def load_database():
    file_list = [
        'data.json',
        'chicago_data.json',
    ]

    data_list = [json.load(open('./static/{}'.format(file))) for file in file_list]
    data_list = sum(data_list, [])

    filtered = []

    for data in data_list:
        if data['objectID'] == 111628:
            print("skip")
            print(data)
            continue
        filtered.append(data)

    data_list = filtered

    return data_list


database = load_database()


def validate(data):
    if 'valid' not in data:
        # print('not checked yet')
        return False
    if not data['valid']:
        # print('invalid')
        return False

    if 'needs_modification' in data and data['needs_modification']:
        # print('needs_modification')
        return False

    return True


database = [data for data in database if validate(data)]


def make_kdtree():
    # 検索システムの構築
    temp_hum = np.array([
        [elem['temp'], elem['hum']] for elem in database
    ])

    # 気温と湿度の効きやすさを平等にするために単位を揃える。
    ss = StandardScaler()
    normed = ss.fit_transform(temp_hum)

    # KDTreeは便利
    tree = KDTree(normed)

    return ss, tree


ss, tree = make_kdtree()    


def query(temp, hum, top):
    # 正規化してから検索
    _, idx = tree.query(ss.transform([[temp, hum]]), k=top)
    return [database[i] for i in idx[0]]


def get_image_url(elem):
    url = ''
    if 'dataset' not in elem:
        # met dataset
        url = request.host_url + "static/images/{}.jpg".format(elem['objectID'])
    elif elem['dataset'] == 'chicago':
        # chicago dataset
        url = request.host_url + "static/chicago_images/{}.jpg".format(elem['objectID'])
    return url

@app.route('/', methods=['GET'])
def index():
    temp = request.args.get('temp')
    try:
        temp = float(temp)
    except:
        return jsonify([{'error': 'temp is invalid: {}'.format(temp)}])
    hum = request.args.get('hum')
    try:
        hum = float(hum)
    except:
        return jsonify([{'error': 'hum is invalid: {}'.format(hum)}])

    top = request.args.get('top')
    try:
        top = int(top)
    except:
        top = 5

    selected = query(temp, hum, top)



    data = [{
        'name': elem['name'],
        'title': elem['title'],
        'date': elem['date'],
        'objectID': elem['objectID'],
        'imageURL': get_image_url(elem),
        'temp': elem['temp'],
        'hum': elem['hum'],
    } for elem in selected]


    return jsonify([{'temp': temp, 'hum': hum, 'data': data}])


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=9040)
