# Python Version

Python 3.x

# Setup

Download images first.

```bash
./download_images.sh
```

Next, install packages

```bash
pip install -r requirements.txt
```

# Usage

Run the server with command below:

```bash
python server.py
```

Then, get artworks with url format like this,

```
http://localhost:9040/?temp=12.12&hum=25.5&top=10
```

Enjoy! :)
